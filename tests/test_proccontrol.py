"""
Unit tests for processing controller.
"""

import logging
import os

import pytest
from ska_sdp_config.entity import ExecutionBlock, Flow, ProcessingBlock, Script
from ska_sdp_config.entity.flow import DataQueue

from ska_sdp_proccontrol import proccontrol
from ska_sdp_proccontrol.utils import get_pb_status

LOG = logging.getLogger(__name__)

MOCK_ENV_VARS = {
    "SDP_CONFIG_BACKEND": "memory",
    "SDP_CONFIG_HOST": "localhost",
    "SDP_HELM_NAMESPACE": "helm",
    "SDP_KAFKA_HOST": "localhost:9092",
}

PROCESSING_BLOCK_ID = "pb-test-20210118-00000"
EXECUTION_BLOCK_ID = "eb-test-20210118-00000"
DEPLOYMENT_ID = f"proc-{PROCESSING_BLOCK_ID}-script"
SCRIPT_KEY = Script.Key(kind="batch", name="test-batch", version="0.2.1")
SCRIPT_IMAGE = "testregistry/script-test-batch:0.2.1"

LAST_UPDATED = "2022-01-01 14:11:12"

SECOND_PB_ID = "pb-test-20220811-00000"
SECOND_EB_ID = "eb-test-20220811-00000"

EB = ExecutionBlock(
    key=EXECUTION_BLOCK_ID,
    pb_realtime=[PROCESSING_BLOCK_ID],
    status="FINISHED",
)

SECOND_EB = ExecutionBlock(
    key=SECOND_EB_ID,
    pb_realtime=[PROCESSING_BLOCK_ID, SECOND_PB_ID],
    status="FINISHED",
)

PB = ProcessingBlock(
    key=PROCESSING_BLOCK_ID,
    eb_id=EXECUTION_BLOCK_ID,
    script=SCRIPT_KEY,
    parameters={},
    dependencies=[],
)

SCRIPT = Script(key=SCRIPT_KEY, image=SCRIPT_IMAGE)


@pytest.mark.parametrize("forward_http", (True, False))
def test_main_loop_start_script(mocker, config, forward_http):
    """
    Test that the ProcessingController.main_loop starts the
    script deployment based on the existing processing blocks
    in the config db.

    This tests that the main_loop starts the deployment if
    the processing block doesn't have a state, i.e. no deployments have
    been started based on it.
    Additionally, it checks that the HTTP proxy environment variables are
    forwarded to the processing script as intended.
    """
    if forward_http:
        http_proxy_vars = {
            "http_proxy": "server",
            "https_proxy": "server",
            "no_proxy": "localhost",
        }
        mocker.patch.dict(os.environ, http_proxy_vars)

    # Perform various actions on the processing block
    # In this case, it will start the script deployment
    # of the just entered processing block
    proccontrol.main_loop(config)

    for txn in config.txn():
        assert get_pb_status(txn, PROCESSING_BLOCK_ID)[0] == "STARTING"
        deployment_ids = txn.deployment.list_keys()

        LOG.info(deployment_ids)
        assert len(deployment_ids) == 1
        # deployment id generated in: ska_sdp_proccontrol.
        # proccontrol.ProcessingController._start_script,
        # based on the pb_id
        assert DEPLOYMENT_ID in deployment_ids

        deployment = txn.deployment.get(DEPLOYMENT_ID)
        assert deployment.args["values"]["image"] == SCRIPT_IMAGE
        assert deployment.args["version"] == "1.0.0"
        deployment_env = {
            var["name"]: var["value"]
            for var in deployment.args["values"]["env"]
        }
        if forward_http:
            assert deployment_env["http_proxy"] == "server"
            assert deployment_env["https_proxy"] == "server"
            assert deployment_env["no_proxy"] == "localhost"
        else:
            assert "http_proxy" not in deployment_env
            assert "https_proxy" not in deployment_env
            assert "no_proxy" not in deployment_env

        # Check for required ENVs, not all values are getting
        # passed through the tests
        assert "SDP_CONFIG_HOST" in deployment_env
        assert "SDP_HELM_NAMESPACE" in deployment_env
        assert "SDP_PB_ID" in deployment_env
        assert deployment_env["SDP_PB_ID"] == PROCESSING_BLOCK_ID
        assert "SDP_DATA_PVC_NAME" in deployment_env
        assert "SDP_KAFKA_HOST" in deployment_env


def test_start_new_pb_scripts(mocker, config):
    """
    ProcessingController._start_new_pb_scripts correctly starts
    a script of a newly added processing block.
    """
    processing_block_ids = [PROCESSING_BLOCK_ID]

    mocker.patch(
        "ska_sdp_proccontrol.proccontrol.get_now",
        mocker.Mock(return_value=LAST_UPDATED),
    )

    for watcher in config.watcher():
        for txn in watcher.txn():
            assert get_pb_status(txn, PROCESSING_BLOCK_ID)[0] is None

        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        for txn in watcher.txn():
            pb_status, pb_state = get_pb_status(txn, PROCESSING_BLOCK_ID)
            assert pb_status == "STARTING"
            assert pb_state["last_updated"] == LAST_UPDATED


@pytest.mark.parametrize(
    "dpl_data, expected_status, error_messages",
    [
        (
            {
                "proc-pb-test-20211111-00001-script-mfnsf": {
                    "error_state": {
                        "reason": "ImagePullErr",
                        "message": "I've failed",
                    },
                    "status": "FAILED",
                }
            },
            "FAILED",
            {
                DEPLOYMENT_ID: {
                    "reason": "ImagePullErr",
                    "message": "I've failed",
                }
            },
        ),
        (
            {
                "proc-pb-test-20211111-00001-script-mfnsf": {
                    "status": "RUNNING"
                }
            },
            "STARTING",
            {},
        ),
    ],
)
def test_monitor_script_deployments(
    dpl_data, expected_status, error_messages, mocker, config
):
    """
    ProcessingController.monitor_script_deployment
    sets pb status to FAILED if its deployment failed,
    else it leaves as STARTING
    """
    processing_block_ids = [PROCESSING_BLOCK_ID]

    mocker.patch(
        "ska_sdp_proccontrol.proccontrol.get_now",
        mocker.Mock(return_value=LAST_UPDATED),
    )

    for watcher in config.watcher():
        for txn in watcher.txn():
            assert get_pb_status(txn, PROCESSING_BLOCK_ID)[0] is None

        # start processing block and create its initial state
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

    for txn in config.txn():
        # create the deployment state that belongs to the PB
        dpl_state = {
            "num_pod": 1,
            "pods": dpl_data,
        }
        txn.deployment.state(DEPLOYMENT_ID).create(dpl_state)

    for watcher in config.watcher():
        for txn in watcher.txn():
            pb_status, pb_state = get_pb_status(txn, PROCESSING_BLOCK_ID)
            assert pb_status == "STARTING"

        proccontrol.monitor_script_deployments(watcher, processing_block_ids)

        for txn in watcher.txn():
            pb_status, pb_state = get_pb_status(txn, PROCESSING_BLOCK_ID)
            assert pb_status == expected_status
            assert pb_state["error_messages"] == error_messages


def test_release_pbs_with_finished_dependencies(mocker, config):
    """
    ProcessingController._release_pbs_with_finished_dependencies correctly
    sets the 'resources_available' state key of the processing block to True
    if the status of the pb is WAITING and all dependencies have finished.

    Here the created pb does not have dependencies. (see fixture)
    """
    processing_block_ids = [PROCESSING_BLOCK_ID]

    mocker.patch(
        "ska_sdp_proccontrol.proccontrol.get_now",
        mocker.Mock(return_value=LAST_UPDATED),
    )

    for watcher in config.watcher():
        # start a script
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        # _release_pbs_with_finished_dependencies works on
        # processing blocks with the following state
        new_state = {"resources_available": False, "status": "WAITING"}
        for txn in watcher.txn():
            txn.processing_block.state(PROCESSING_BLOCK_ID).update(new_state)

        proccontrol.release_pbs_with_finished_dependencies(
            watcher, processing_block_ids
        )

        expected_pb_state = {
            "resources_available": True,
            "status": "WAITING",
            "last_updated": LAST_UPDATED,
        }
        for txn in watcher.txn():
            assert (
                txn.processing_block.state(PROCESSING_BLOCK_ID).get()
                == expected_pb_state
            )


def test_delete_deployments_without_pb(config):
    """
    ProcessingControl._delete_deployments_without_pb successfully removes
    deployments without a processing block
    """

    for watcher in config.watcher():
        # start a script
        proccontrol.start_new_pb_scripts(watcher, [PROCESSING_BLOCK_ID])

        # remove the processing block, but leave the deployment
        config.backend.delete("/pb", must_exist=False, recursive=True)

        for txn in watcher.txn():
            assert len(txn.deployment.list_keys()) == 1

        proccontrol.delete_deployments_without_pb(watcher, [], [DEPLOYMENT_ID])

        for txn in watcher.txn():
            assert len(txn.deployment.list_keys()) == 0


def test_delete_flows_without_pb(config):
    """
    ProcessingControl._delete_flows_without_pb successfully removes
    data flow entries without a processing block

    Here we create two flows, one for each pb in the db. Then remove
    one of the pbs and show that the associated flow has also been removed
    """
    second_pb = PB.model_copy(deep=True)
    second_pb.key = SECOND_PB_ID
    second_pb.eb_id = SECOND_EB_ID

    flow_key = Flow.Key(
        pb_id=PROCESSING_BLOCK_ID, kind="data-queue", name="my-test-flow"
    )
    second_flow_key = Flow.Key(
        pb_id=SECOND_PB_ID, kind="data-queue", name="my-second-flow"
    )
    flow_sink = DataQueue(
        topics="test-topic", host="kafka://test-host:9092", format="json"
    )
    flow = Flow(
        key=flow_key, sink=flow_sink, data_model="TestModel", sources=[]
    )
    second_flow = Flow(
        key=second_flow_key, sink=flow_sink, data_model="TestModel", sources=[]
    )

    for txn in config.txn():
        # pb with id PROCESSING_BLOCK_ID has already been created
        # as part of the call to the config fixture
        txn.flow.create(flow)
        txn.flow.create(second_flow)
        txn.processing_block.create(second_pb)
        txn.execution_block.create(SECOND_EB)

    for txn in config.txn():
        flows_in_db = txn.flow.list_keys()
        assert flows_in_db == [flow_key, second_flow_key]

        # remove one processing block, but leave its flow object
        txn.processing_block.delete(PROCESSING_BLOCK_ID, recurse=True)

    for watcher in config.watcher():
        proccontrol.delete_flows_without_pb(
            watcher, [SECOND_PB_ID], [flow_key]
        )

    for txn in config.txn():
        flows_in_db = txn.flow.list_keys()
        assert flows_in_db == [second_flow_key]


def test_delete_finished_ebs_and_related_pbs_keep(mocker, config):
    """
    EB status is FINISHED, it has two related PBs.
    First PB status is FINISHED and there isn't a last_udpated key.
    Second PB status is FINISHED, there is a last_updated key in state,
    but time between that and now hasn't passed the required delay.
    Though 1st PB could be deleted, the 2nd can't, so neither of them
    are, nor the EB are deleted.
    """
    processing_block_ids = [PROCESSING_BLOCK_ID, SECOND_PB_ID]
    execution_block_ids = [SECOND_EB_ID]

    second_pb = PB.model_copy(deep=True)
    second_pb.key = SECOND_PB_ID
    second_pb.eb_id = SECOND_EB_ID

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.processing_block.create(second_pb)
            txn.execution_block.create(SECOND_EB)

        # start a script
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        new_state_pb = {
            "resources_available": False,
            "status": "FINISHED",
        }
        new_state_second_pb = {
            "resources_available": False,
            "status": "FINISHED",
            "last_updated": "2022-08-01 10:10:02",
        }
        for txn in watcher.txn():
            txn.processing_block.state(PROCESSING_BLOCK_ID).update(
                new_state_pb
            )
            txn.processing_block.state(SECOND_PB_ID).update(
                new_state_second_pb
            )

            pbs = txn.processing_block.list_keys()
            ebs = txn.execution_block.list_keys()
            assert PROCESSING_BLOCK_ID in pbs
            assert SECOND_PB_ID in pbs
            assert EXECUTION_BLOCK_ID in ebs

        mock_now = mocker.patch("ska_sdp_proccontrol.utils.get_now")
        mock_now.return_value = "2022-08-01 10:40:02"
        proccontrol.delete_finished_ebs_and_related_pbs(
            watcher, execution_block_ids, delay=3600
        )
        mock_now.stop()

        for txn in watcher.txn():
            pbs = txn.processing_block.list_keys()
            ebs = txn.execution_block.list_keys()
            assert PROCESSING_BLOCK_ID in pbs
            assert SECOND_PB_ID in pbs
            assert EXECUTION_BLOCK_ID in ebs


def test_delete_finished_ebs_and_related_pbs_delete(mocker, config):
    """
    EB status is FINISHED, it has two related PBs.
    Both PBs are FINISHED, there is a last_updated key in state,
    and the time between that and now has passed the required delay
    for both of them. Both PBs and EB are deleted.
    """
    processing_block_ids = [PROCESSING_BLOCK_ID, SECOND_PB_ID]
    execution_block_ids = [SECOND_EB_ID]

    second_pb = PB.model_copy(deep=True)
    second_pb.key = SECOND_PB_ID
    second_pb.eb_id = SECOND_EB_ID

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.processing_block.create(second_pb)
            txn.execution_block.create(SECOND_EB)

        # start a script
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        new_state = {
            "resources_available": False,
            "status": "FINISHED",
            "last_updated": "2022-08-01 10:10:02",
        }
        eb_finished_state = {
            "status": "FINISHED",
        }
        for txn in watcher.txn():
            txn.processing_block.state(PROCESSING_BLOCK_ID).update(new_state)
            txn.processing_block.state(SECOND_PB_ID).update(new_state)
            txn.execution_block.state(SECOND_EB_ID).create(eb_finished_state)

            pbs = txn.processing_block.list_keys()
            ebs = txn.execution_block.list_keys()
            assert PROCESSING_BLOCK_ID in pbs
            assert SECOND_PB_ID in pbs
            assert EXECUTION_BLOCK_ID in ebs

        mock_now = mocker.patch("ska_sdp_proccontrol.utils.get_now")
        mock_now.return_value = "2022-08-01 10:50:02"
        proccontrol.delete_finished_ebs_and_related_pbs(
            watcher, execution_block_ids, delay=1800
        )
        mock_now.stop()

        for txn in config.txn():
            pbs = txn.processing_block.list_keys()
            ebs = txn.execution_block.list_keys()
            assert PROCESSING_BLOCK_ID not in pbs
            assert SECOND_PB_ID not in pbs
            assert SECOND_EB_ID not in ebs


def test_delete_finished_pb_without_eb(config):
    """
    Processing Block without associated execution block is deleted,
    if status is FINISHED.
    """
    processing_block_ids = [PROCESSING_BLOCK_ID, SECOND_PB_ID]
    new_pb = PB.model_copy(deep=True)
    new_pb.key = SECOND_PB_ID
    new_pb.eb_id = None

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.processing_block.create(new_pb)

        # start a scripts
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        new_state = {
            "resources_available": False,
            "status": "FINISHED",
        }
        for txn in watcher.txn():
            # Need status to be FINISHED
            txn.processing_block.state(SECOND_PB_ID).update(new_state)
            pb = txn.processing_block.get(SECOND_PB_ID)
            assert pb is not None
            assert pb.eb_id is None

        proccontrol.delete_finished_pb_without_eb(
            watcher, processing_block_ids
        )

        for txn in watcher.txn():
            # PB with EB stays in DB, but PB without EB is deleted
            pbs = txn.processing_block.list_keys()
            assert PROCESSING_BLOCK_ID in pbs
            assert SECOND_PB_ID not in pbs


def test_delete_finished_pb_without_eb_dead_eb(config):
    """
    Processing Block has execution block associated with it,
    but the EB doesn't exist in the DB anymore.
    Delete the PB.
    """
    processing_block_ids = [PROCESSING_BLOCK_ID]

    for watcher in config.watcher():
        # start a scripts
        proccontrol.start_new_pb_scripts(watcher, processing_block_ids)

        new_state = {
            "resources_available": False,
            "status": "FINISHED",
        }
        for txn in watcher.txn():
            # Need status to be FINISHED
            txn.processing_block.state(PROCESSING_BLOCK_ID).update(new_state)
            pb = txn.processing_block.get(PROCESSING_BLOCK_ID)
            assert pb is not None

            txn.execution_block.delete(pb.eb_id)
            ebs = txn.execution_block.list_keys()
            # PB has associated EB but that is not in the DB
            assert pb.eb_id is not None
            assert pb.eb_id not in ebs

        proccontrol.delete_finished_pb_without_eb(
            watcher, processing_block_ids
        )

        for txn in watcher.txn():
            pbs = txn.processing_block.list_keys()
            assert PROCESSING_BLOCK_ID not in pbs


def test_delete_execution_blocks_without_pb_empty_list(config):
    """
    An execution block whose pb_realtime and pb_batch lists are empty,
    is deleted from the Config DB. Status is not checked.
    """
    eb_ids = [EXECUTION_BLOCK_ID, SECOND_EB_ID]
    eb = ExecutionBlock(key=SECOND_EB_ID)

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.execution_block.create(eb)

            ebs = txn.execution_block.list_keys()
            assert SECOND_EB_ID in ebs

        proccontrol.delete_execution_blocks_without_pb(watcher, eb_ids)

        for txn in watcher.txn():
            ebs = txn.execution_block.list_keys()
            # eb with existing pb is not deleted
            assert EXECUTION_BLOCK_ID in ebs
            # eb is deleted
            assert SECOND_EB_ID not in ebs


def test_delete_execution_blocks_without_pb_dead_pb(config):
    """
    Execution block has processing blocks associated with it,
    but those pbs don't exist any more. The eb is deleted from
    the Config DB. Status is not checked.
    """
    eb_ids = [EXECUTION_BLOCK_ID, SECOND_EB_ID]

    eb = SECOND_EB.model_copy()
    eb.pb_realtime = [SECOND_PB_ID]

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.execution_block.create(eb)

            ebs = txn.execution_block.list_keys()
            pbs = txn.processing_block.list_keys()
            assert SECOND_EB_ID in ebs
            assert (
                SECOND_PB_ID not in pbs
            )  # PB associated with new_ib, doesn't exist

        proccontrol.delete_execution_blocks_without_pb(watcher, eb_ids)

        for txn in watcher.txn():
            ebs = txn.execution_block.list_keys()
            # eb is deleted
            assert SECOND_EB_ID not in ebs


def test_delete_execution_blocks_without_pb_existing_pb(config):
    """
    Execution block has processing blocks associated with it,
    and at least one of the pbs still exists in the DB.
    The eb is not deleted.
    """
    eb_ids = [EXECUTION_BLOCK_ID, SECOND_EB_ID]

    for watcher in config.watcher():
        for txn in watcher.txn():
            txn.execution_block.create(SECOND_EB)

            ebs = txn.execution_block.list_keys()
            pbs = txn.processing_block.list_keys()
            assert SECOND_EB_ID in ebs
            assert SECOND_PB_ID not in pbs  # first PB doesn't exist
            assert PROCESSING_BLOCK_ID in pbs  # second PB is in the DB

        proccontrol.delete_execution_blocks_without_pb(watcher, eb_ids)

        for txn in watcher.txn():
            ebs = txn.execution_block.list_keys()
            # eb is not deleted
            assert SECOND_EB_ID in ebs


def test_main(mocker):
    """
    Test that the main function executes without any errors.
    """
    mocker.patch("signal.signal", mocker.Mock())
    mocker.patch("sys.exit", mocker.Mock())

    proccontrol.main()
