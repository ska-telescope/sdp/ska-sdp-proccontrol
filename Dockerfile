FROM artefact.skao.int/ska-build-python:0.1.1 AS build

WORKDIR /build

COPY . ./

ENV POETRY_VIRTUALENVS_CREATE=false

RUN poetry install --no-root --only main
RUN pip install --no-compile .

FROM artefact.skao.int/ska-python:0.1.2

COPY --from=build /usr/local/ /usr/local/

WORKDIR /app

ENTRYPOINT ["ska-sdp-proccontrol"]
