include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-proccontrol
PROJECT_PATH = ska-telescope/sdp/ska-sdp-proccontrol
ARTEFACT_TYPE = oci

CHANGELOG_FILE = CHANGELOG.rst