"""Configuration for tests."""

import os

import pytest
import ska_sdp_config
from ska_ser_logging import configure_logging
from test_proccontrol import EB, MOCK_ENV_VARS, PB, SCRIPT

configure_logging()


@pytest.fixture(autouse=True)
def fixture_environment_variables(mocker):
    """Patch environment variables."""
    mocker.patch.dict(os.environ, MOCK_ENV_VARS)


@pytest.fixture(name="config")
def fixture_config():
    """
    Fixture to create config object with a script definition and processing
    block in the config DB.
    """
    config = ska_sdp_config.Config(owned_entity=("component", "proccontrol"))

    for txn in config.txn():
        txn.script.create(SCRIPT)
        txn.processing_block.create(PB)
        txn.execution_block.create(EB)

    yield config

    entities = ["/script", "/pb", "/deploy", "/eb", "/component", "/flow"]
    for entity in entities:
        config.backend.delete(entity, must_exist=False, recursive=True)
