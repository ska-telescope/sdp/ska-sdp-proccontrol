"""
Util functions to be used with the Processing Controller
"""

import datetime
import logging

LOG = logging.getLogger(__name__)
TIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def get_now():
    """
    Return timestamp for "now"
    """
    return datetime.datetime.now().strftime(TIME_FORMAT)


def compare_timestamps(start, end, delay):
    """
    Compare two timestamps.
    Returns True if end > start + delay (delay in seconds)
    """
    start_to_datetime = datetime.datetime.strptime(start, TIME_FORMAT)
    end_to_datetime = datetime.datetime.strptime(end, TIME_FORMAT)
    delay_delta = datetime.timedelta(seconds=delay)
    return end_to_datetime >= start_to_datetime + delay_delta


def get_pb_status(txn, pb_id):
    """
    Get status and state of processing block.

    :param txn: config DB transaction
    :param pb_id: processing block ID
    :returns: processing block status and state
    """
    state = txn.processing_block.state(pb_id).get()
    if state is None:
        status = None
    else:
        status = state.get("status")
    return status, state


def can_delete_pb(watcher, txn, pb_id, delay):
    """
    Check whether a processing block can be deleted
    from the Configuration database or not.

    :param watcher: watcher object; needed to set a wake up
                    of the watcher, if a finished processing
                    block hasn't reached its time-to-delete yet.
    :param txn: config DB transaction
    :param pb_id: ID of processing block to be deleted
    :param delay: delay in seconds between finishing a PB
                  and when it needs deleting
    """
    pb = txn.processing_block.get(pb_id)
    if pb is None:
        return True

    status, state = get_pb_status(txn, pb_id)

    if status == "FINISHED":
        last_updated = state.get("last_updated", None)
        if last_updated is None:
            return True

        if compare_timestamps(last_updated, get_now(), delay):
            return True

        # if pb hasn't reached its to-delete time (i.e. above if statement),
        # the watcher needs another trigger to wake; we set it here
        last_updated = datetime.datetime.strptime(last_updated, TIME_FORMAT)
        wake_up = last_updated + datetime.timedelta(seconds=delay)
        watcher.set_wake_up_at(wake_up)

    return False
