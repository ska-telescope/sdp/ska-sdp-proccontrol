"""
Main processing controller functions, including the event loop.
"""

import logging
import os
import re
import signal
import sys
from copy import deepcopy
from pathlib import Path

import ska_sdp_config
from ska_sdp_config.entity import Deployment
from ska_ser_logging import configure_logging

from .utils import can_delete_pb, get_now, get_pb_status

CONFIG_HOST = os.getenv("SDP_CONFIG_HOST")
HELM_NAMESPACE = os.getenv("SDP_HELM_NAMESPACE")
SDP_KAFKA_HOST = os.getenv("SDP_KAFKA_HOST")

# Time interval in seconds for idle loop of the Processing Controller as
# used for Kubernetes liveness probe
LOOP_INTERVAL = int(os.getenv("SDP_PROCCONTROL_LOOP_INTERVAL", "60"))
# Liveness file name - must match the name used in the Kubernetes
# Pod yaml probe configuration!
LIVENESS_FILE = Path(os.getenv("SDP_PROCCONTROL_LIVENESS_FILE", "/tmp/alive"))

# Allow changing the chart version that is deployed for processing scripts
# Default to version 1.0.0 if it's not given.
SDP_SCRIPT_CHART_VERSION = os.getenv("SDP_SCRIPT_CHART_VERSION", "1.0.0")

DATA_PVC_NAME = os.getenv("SDP_DATA_PVC_NAME")
LOG_LEVEL = os.getenv("SDP_LOG_LEVEL", "DEBUG")

configure_logging(level=LOG_LEVEL)
LOG = logging.getLogger(__name__)

# Regular expression to match processing block ID as substring
_RE_PB = "pb(-[0-9a-zA-Z]*){3}"

# Compiled regular expression to match processing deployments associated with
# a processing block
_RE_DEPLOY_PROC_ANY = re.compile(f"^proc-(?P<pb_id>{_RE_PB}).*$")
DPL_ID_PATTERN = "proc-{pb_id}-script"


def start_new_pb_scripts(watcher, pb_ids):
    """
    Start the scripts for new processing blocks.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    """
    for pb_id in pb_ids:
        for txn in watcher.txn():
            if txn.processing_block.get(pb_id) is None:
                break

            state = txn.processing_block.state(pb_id).get()
            if state is None:
                start_script(txn, pb_id)


def start_script(txn, pb_id):
    """
    Start the script for a processing block.

    :param txn: config DB transaction
    :param pb_id: processing block ID

    """
    LOG.info("Making deployment for processing block %s", pb_id)

    # Read the processing block
    pb = txn.processing_block.get(pb_id)

    # Get the container image for the script
    script = txn.script.get(pb.script)
    if script is None:
        image = None
    else:
        image = script.image

    if image is not None:
        # Make the deployment
        LOG.info("Deploying script %s", pb.script)
        deploy_id = DPL_ID_PATTERN.format(pb_id=pb_id)

        values = {
            "image": image,
            "env": [
                {"name": "SDP_CONFIG_HOST", "value": CONFIG_HOST},
                {"name": "SDP_HELM_NAMESPACE", "value": HELM_NAMESPACE},
                {"name": "SDP_PB_ID", "value": pb_id},
                {"name": "SDP_DATA_PVC_NAME", "value": DATA_PVC_NAME},
                {"name": "SDP_KAFKA_HOST", "value": SDP_KAFKA_HOST},
            ],
        }
        # HTTP proxy settings. These env vars are not always present,
        # so we forward them only if given
        for name in ("http_proxy", "https_proxy", "no_proxy"):
            value = os.getenv(name)
            if value is not None:
                values["env"].append({"name": name, "value": value})
        release = {
            "chart": "script",
            "values": values,
            "version": SDP_SCRIPT_CHART_VERSION,
        }
        deploy = Deployment(key=deploy_id, kind="helm", args=release)
        txn.deployment.create(deploy)
        # Set status to STARTING, and resources_available to False
        state = {
            "status": "STARTING",
            "resources_available": False,
            "error_messages": {},
            "last_updated": get_now(),
        }
    else:
        # Invalid script, so set status to FAILED
        state = {
            "status": "FAILED",
            "error_messages": {"reason": f"No image for script {pb.script}"},
            "last_updated": get_now(),
        }
        LOG.error(
            "Deployment for processing block %s could not be started, "
            "because no image could be found for script %s.",
            pb_id,
            pb.script,
        )

    # Create the processing block state.
    txn.processing_block.state(pb_id).create(state)


def monitor_script_deployments(watcher, pb_ids):
    """
    If processing script deployment fails, set the processing
    block state to FAILED and provide error message.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    """
    # pylint: disable-next=too-many-nested-blocks
    for pb_id in pb_ids:
        deploy_id = DPL_ID_PATTERN.format(pb_id=pb_id)

        for txn in watcher.txn():
            dpl_state = txn.deployment.state(deploy_id).get()
            if dpl_state:
                pod_data = dpl_state.get("pods")
                if pod_data:
                    _, data = pod_data.popitem()
                    if data.get("status", "") == "FAILED":
                        pb_state = txn.processing_block.state(pb_id).get()
                        if pb_state:
                            original_state = deepcopy(pb_state)
                            pb_state["status"] = "FAILED"
                            pb_state["error_messages"] = {
                                deploy_id: data.get("error_state")
                            }
                            if original_state != pb_state:
                                log_message = {
                                    deploy_id: data.get("error_state", {}).get(
                                        "message"
                                    )
                                }
                                LOG.error(
                                    "Processing script with id %s failed "
                                    "to start. Reason: %s",
                                    pb_id,
                                    log_message,
                                )
                                txn.processing_block.state(pb_id).update(
                                    pb_state
                                )


def release_pbs_with_finished_dependencies(watcher, pb_ids):
    """
    Release processing blocks whose dependencies are all finished.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    """
    for pb_id in pb_ids:
        for txn in watcher.txn():
            if txn.processing_block.get(pb_id) is None:
                break

            state = txn.processing_block.state(pb_id).get()
            if state is None:
                status = None
                ra = None
            else:
                status = state.get("status")
                ra = state.get("resources_available")
            if status == "WAITING" and not ra:
                # Check status of dependencies.
                pb = txn.processing_block.get(pb_id)
                if pb.script.kind == "realtime" or all(
                    get_pb_status(txn, dep.pb_id)[0] == "FINISHED"
                    for dep in pb.dependencies
                ):
                    LOG.info("Releasing processing block %s", pb_id)
                    state["resources_available"] = True
                    state["last_updated"] = get_now()
                    txn.processing_block.state(pb_id).update(state)


def delete_finished_pb_without_eb(watcher, pb_ids, delay=600):
    """
    Remove processing blocks if they were successful and
    have been in the database for <delay> seconds.

    Only remove processing blocks without associated
    execution blocks.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    :param delay: delay in seconds between finishing a PB
                    and when it needs deleting
    """
    for pb_id in pb_ids:
        for txn in watcher.txn():
            pb = txn.processing_block.get(pb_id)
            if pb is None:
                break
            eb_id = pb.eb_id

            if eb_id is None:
                if can_delete_pb(watcher, txn, pb_id, delay):
                    LOG.info("Deleting processing block %s", pb_id)
                    txn.processing_block.delete(pb_id, recurse=True)

            else:
                eb = txn.execution_block.get(eb_id)
                if eb is None:
                    if can_delete_pb(watcher, txn, pb_id, delay):
                        LOG.info("Deleting processing block %s", pb_id)
                        txn.processing_block.delete(pb_id, recurse=True)


def delete_finished_ebs_and_related_pbs(watcher, eb_ids, delay=600):
    """
    Remove execution blocks if they are finished and all
    associated processing blocks are finished and can
    also be deleted.

    A processing block can be deleted if its status is FINISHED,
    and it has been in the database for longer than <delay> seconds.

    :param watcher: config DB watcher object (Config.watcher())
    :param eb_ids: list of execution block ids
    :param delay: delay in seconds between finishing a PB
                    and when it can be deleted
    """
    for eb_id in eb_ids:
        for txn in watcher.txn():
            eb = txn.execution_block.get(eb_id)
            if eb is None:
                break
            eb_state = txn.execution_block.state(eb_id).get()
            eb_status = (
                eb_state.get("status") if eb_state is not None else None
            )
            if eb_status == "FINISHED":
                all_pb_ids = eb.pb_batch + eb.pb_realtime

                to_delete = []
                for pb_id in all_pb_ids:
                    if can_delete_pb(watcher, txn, pb_id, delay):
                        to_delete.append(pb_id)

                if sorted(to_delete) == sorted(all_pb_ids):
                    for pb_id in to_delete:
                        LOG.info("Deleting processing block %s", pb_id)
                        txn.processing_block.delete(pb_id, recurse=True)

                    LOG.info("Deleting execution block %s", eb_id)
                    txn.execution_block.delete(eb_id, recurse=True)

                else:
                    LOG.info(
                        "Execution block %s still has associated "
                        "processing blocks that cannot be deleted.",
                        eb_id,
                    )


def delete_execution_blocks_without_pb(watcher, eb_ids):
    """
    Remove execution blocks without any associated
    processing blocks.

    :param watcher: config DB watcher object (Config.watcher())
    :param eb_ids: list of execution block ids
    """
    for eb_id in eb_ids:
        for txn in watcher.txn():

            eb = txn.execution_block.get(eb_id)
            if eb is None:
                break

            if all(
                txn.processing_block.get(pb_id) is None
                for pb_id in eb.pb_batch + eb.pb_realtime
            ):
                LOG.info("Deleting execution block %s", eb_id)
                txn.execution_block.delete(eb_id, recurse=True)


def delete_deployments_without_pb(watcher, pb_ids, deploy_ids):
    """
    Delete processing deployments not associated with a processing block.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    :param deploy_ids: list of deployment ids
    """
    for deploy_id in deploy_ids:
        match = _RE_DEPLOY_PROC_ANY.match(deploy_id)
        pb_id = None
        if match is not None:
            pb_id = match.group("pb_id")

        if pb_id and pb_id not in pb_ids:
            for txn in watcher.txn():
                if txn.deployment.get(deploy_id) is None:
                    break

                LOG.info("Deleting deployment %s", deploy_id)
                txn.deployment.delete(deploy_id, recurse=True)


def delete_flows_without_pb(watcher, pb_ids, flow_ids):
    """
    Delete data flows not associated with a processing block.

    :param watcher: config DB watcher object (Config.watcher())
    :param pb_ids: list of processing block ids
    :param flow_ids: list of data flow ids in the Config DB
    """
    for flow_id in flow_ids:
        pb_id = flow_id.pb_id
        if pb_id and pb_id not in pb_ids:
            for txn in watcher.txn():
                if txn.flow.get(flow_id) is None:
                    break

                LOG.info("Deleting flow %s", flow_id)
                txn.flow.delete(flow_id, recurse=True)


def main_loop(config):
    """
    Main event loop, which performs multiple actions on the configuration.

    :param config: configuration DB client
    """
    # Create owner entry
    for txn in config.txn():
        txn.self.ownership.take()

    # Set main loop timeout
    timeout = LOOP_INTERVAL
    LOG.debug("Main loop timeout is %d seconds", timeout)

    LOG.info("Starting main loop")
    for watcher in config.watcher(timeout=timeout):
        # Update liveness file
        LIVENESS_FILE.touch()

        # Check owner entry
        for txn in watcher.txn():
            txn.self.take_ownership_if_not_alive()

        # List processing blocks, execution blocks, and deployments
        for txn in watcher.txn():
            pb_ids = txn.processing_block.list_keys()
            eb_ids = txn.execution_block.list_keys()
            LOG.info("processing block ids %s", pb_ids)

        # Perform actions
        start_new_pb_scripts(watcher, pb_ids)
        release_pbs_with_finished_dependencies(watcher, pb_ids)
        monitor_script_deployments(watcher, pb_ids)
        delete_finished_ebs_and_related_pbs(watcher, eb_ids, delay=3600)
        delete_finished_pb_without_eb(watcher, pb_ids, delay=3600)
        delete_execution_blocks_without_pb(watcher, eb_ids)

        # requery pb_ids since some of the above functions may have
        # removed some already
        for txn in watcher.txn():
            pb_ids = txn.processing_block.list_keys()
            deploy_ids = txn.deployment.list_keys()
            flow_ids = txn.flow.list_keys()

        delete_deployments_without_pb(watcher, pb_ids, deploy_ids)
        delete_flows_without_pb(watcher, pb_ids, flow_ids)


def terminate(_signal, _frame):
    """Terminate the program."""
    LOG.info("Asked to terminate")
    sys.exit(0)


def main(backend=None):
    """
    Start the processing controller.

    :param backend: config DB backend
    """
    # Register signal handler
    signal.signal(signal.SIGTERM, terminate)

    # Connect to config DB
    LOG.info("Connecting to config DB")
    config = ska_sdp_config.Config(
        backend=backend, owned_entity=("component", "proccontrol")
    )

    try:
        # Start main event loop
        main_loop(config)
    finally:
        # close config DB connection
        config.close()
