"""
Unit tests for utils.py
"""

import pytest
from test_proccontrol import PROCESSING_BLOCK_ID

from ska_sdp_proccontrol import proccontrol
from ska_sdp_proccontrol.utils import can_delete_pb, compare_timestamps


@pytest.mark.parametrize(
    "start, end, delay, expected",
    [
        ("2022-01-01 10:00:10", "2022-01-01 23:00:10", 7200, True),
        ("2022-01-01 10:00:10", "2022-01-01 10:50:10", 4200, False),
        ("2022-01-01 10:00:10", "2022-02-01 09:00:10", 7200, True),
    ],
)
def test_compare_timestamps(start, end, delay, expected):
    """Test different timestamp comparisons"""
    result = compare_timestamps(start, end, delay)
    assert result is expected


@pytest.mark.parametrize(
    "status, last_updated, expected",
    [
        ("FINISHED", None, True),
        ("RUNNING", None, False),
        ("FINISHED", "2022-08-01 10:00:02", True),
        ("FINISHED", "2022-08-01 10:30:02", False),
    ],
)
def test_can_delete_pb(mocker, config, status, last_updated, expected):
    """
    Test that function correctly informs about whether
    a processing block should be deleted from the DB
    or not.
    """
    for watcher in config.watcher():
        proccontrol.start_new_pb_scripts(watcher, [PROCESSING_BLOCK_ID])
        for txn in watcher.txn():
            txn.processing_block.state(PROCESSING_BLOCK_ID).update(
                {"status": status, "last_updated": last_updated},
            )

            mock_now = mocker.patch("ska_sdp_proccontrol.utils.get_now")
            mock_now.return_value = "2022-08-01 10:40:02"
            result = can_delete_pb(watcher, txn, PROCESSING_BLOCK_ID, 1800)
            assert result is expected
            mock_now.stop()
