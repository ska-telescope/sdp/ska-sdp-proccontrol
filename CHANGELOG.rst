Changelog
=========

1.0.0
-----

-  Updated dependencies for list, see pyproject.toml on the MR
   (`MR70 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/70>`__)
-  Use SKA theme in documentation
   (`MR69 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/69>`__)
-  Removed broken links
   (`MR67 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/67>`__)  
-  Use CAR chart
   (`MR68 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/68>`__)  
-  Update Dockerfile to use SKA Python base image
   (`MR64 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/64>`__)

0.16.1
------

-  Use ska-sdp-config 0.10.2
   (`MR65 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/65>`__)

   -  Fixes SKB-685: txn.update is only committed if there is a
      difference between existing and new value

-  Fixes SKB-685: Only update pb_state when script fails if there is a
   change in existing and new pb_state
   (`MR65 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/65>`__)

.. _section-1:

0.16.0
------

-  Use ska-sdp-config 0.10.1
   (`MR63 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/63>`__)
-  Monitor script deployment and report error in pb state with status
   FAILED
   (`MR62 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/62>`__)
-  Allow setting script chart version via environment variable (default
   to v0.1.0)
   (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/61>`__)

.. _section-2:

0.15.0
------

-  Update ska-sdp-config to 0.9.0
   (`MR57 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/57>`__)
-  Update to use pydantic EB model
   (`MR55 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/55>`__)
-  Switched to use ska-sdp-python base image
   (`MR56 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/56>`__)

.. _section-3:

0.14.0
------

-  Update ska-sdp-config to 0.6.0 and port code to use new API
   (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/51>`__).

.. _section-4:

0.13.0
------

-  Refactor code to remove top-level processing controller class
   (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/51>`__)

.. _section-5:

0.12.3
------

-  Update ska-sdp-config to v0.5.6 and close connection to Configuration
   DB upon exit
   (`MR47 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/47>`__)

.. _section-6:

0.12.2
------

-  Allow dependencies between real-time processing blocks to exist
   without affecting their scheduling
   (`MR40 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/40>`__).
-  Added logic to handle the cases where an entity has vanished is not
   correct
   (`MR41 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/41>`__)
-  Added ``SDP_KAFKA_HOST`` environment variable
   (`MR38 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/38>`__)

.. _section-7:

0.12.1
------

-  Updated to use the new version of config DB 0.5.3 which includes a
   bugfix in delete functionality
   (`MR36 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/36>`__)

.. _section-8:

0.12.0
------

-  Updated to use the new version of config DB 0.5.2
   (`MR35 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/35>`__)

.. _section-9:

0.11.5
------

-  Add forwarding of http proxy environment variables to processing
   scripts
   (`MR30 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/30>`__)

.. _section-10:

0.11.4
------

-  ska-sdp-config == 0.4.4
   (`MR29 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/29>`__)

.. _section-11:

0.11.3
------

-  Add exit handler and updated component to generate lease entry
   (alive_key)
   (`MR26 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/26>`__)
-  Add idle looping with updating of flag file to integrate with
   Kubernetes liveness probing
   (`MR27 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/27>`__)
-  Pass PVC information onto processing scripts via environment variable
   (`MR25 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/25>`__)

.. _section-12:

0.11.2
------

-  If PB ID does not match regex, set its status to FAILED
   (`MR23 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/23>`__)
-  Documentation updates
   (`MR22 <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol/-/merge_requests/22>`__)

.. _section-13:

0.10.1
------

-  Add reason to processing block state when workflow cannot be
   deployed.
-  Publish Docker image in central artefact repository.

.. _section-14:

0.10.0
------

-  Read workflow definitions from configuration database.
